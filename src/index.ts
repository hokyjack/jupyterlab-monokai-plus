// Copyright (c) Itay B. Dafna.
// Distributed under the terms of the Modified BSD License.

import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { IThemeManager } from '@jupyterlab/apputils';

/**
 * A plugin for jupyterlab-monokai-plus
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'jupyterlab-monokai-plus:plugin',
  requires: [IThemeManager],
  activate: (app: JupyterFrontEnd, manager: IThemeManager) => {
    const style = '@hokyjack/jupyterlab-monokai-plus/index.css';

    manager.register({
      name: 'Monokai++',
      isLight: false,
      themeScrollbars: true,
      load: () => manager.loadCSS(style),
      unload: () => Promise.resolve(undefined)
    });
  },
  autoStart: true
};

export default plugin;
