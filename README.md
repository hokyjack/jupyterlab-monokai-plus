# Monokai++ Theme

A Monokai++ theme extension for JupyterLab.  
If you like pitch black backgrounds with high contrast colours, this theme is for you.

This theme derives from [Dark Monokai theme](https://github.com/ibdafna/theme-dark-monokai).  
It has different code color syntax, slightly brighter background and font of size 13px.

**_Important: Monokai++ is compatible with JupyterLab version 1.0.0 and higher_**

![monokai_plus_image](monokai_plus.png)

## Prerequisites

* JupyterLab

## Installation

```bash
jupyter labextension install @hokyjack/jupyterlab-monokai-plus
```

## Development

For a development install (requires npm version 4 or later), do the following in the repository directory:

```bash
npm install
jupyter labextension link .
```

To rebuild the package and the JupyterLab app:

```bash
npm run build
jupyter lab build
```
